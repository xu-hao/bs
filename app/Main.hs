{-# LANGUAGE TemplateHaskell #-}
module Main where

import Graphics.UI.GLUT
import Graphics.UI.GLUT.Window
import System.Exit
import Data.Time.Clock
import Data.Time.LocalTime
import Data.IORef
import Data.Colour.RGBSpace as C
import Data.Colour.RGBSpace.HSV (hsv)
import Options.Applicative
import Data.Semigroup ((<>))
import Control.Monad (forever, void)
import Control.Lens
import Control.Concurrent.Async.Timer (defaultConf, setInitDelay, setInterval, withAsyncTimer, wait, reset)
import Control.Concurrent.Async (async)

myVertices :: Float -> [(Float, Float)]
myVertices radius = let n = 60 in map (\h -> let alpha = pi / 2 - 2 * pi * h/n in (radius * cos alpha, radius * sin alpha)) [0..n-1]

tickVertices :: Float -> [(Float, Float)]
tickVertices radius = let n = 12 in concatMap (\h -> let alpha = pi / 2 - 2 * pi * h/n in [(0.95 * radius * cos alpha, 0.95 * radius * sin alpha),(radius * cos alpha, radius * sin alpha)]) [0..n-1]

hourArmLength, minuteArmLength, secondArmLength :: Float -> Float
hourArmLength radius = 0.5 * radius
minuteArmLength radius = 0.7 * radius
secondArmLength radius = 0.75 * radius

scalex, scaley, scalez :: Float
scalex = 0.1
scaley = 0.1
scalez = 0.1

ccolor :: Int -> Color3 Float
ccolor s =
    let (C.RGB r g b) = hsv (fromIntegral s / 60 * 360) 1 0.5 in
        Color3 r g b

data Option = Option {
    _clock :: Bool,
    _changeColor :: Bool,
    _rgb :: (Float, Float, Float),
    _hide :: Bool,
    _clockScale :: Float,
    _showCursor :: Bool,
    _showCursorInterval :: Int
}

data ApplicationState = ApplicationState {
    _applicationOption :: Option,
    _applicationTimeOfDay :: TimeOfDay,
    _showClock :: Bool
}

makeLenses ''Option
makeLenses ''ApplicationState
makeLensesFor [("todSec", "second"), ("todMin", "minute"), ("todHour", "hour")] ''TimeOfDay

bsoption :: Parser Option
bsoption = Option
    <$> switch (short 'c' <> long "clock" <> help "show clock")
    <*> switch (short 'C' <> long "changeColor" <> help "change color")
    <*> option auto (short 'l' <> long "rgb" <> value (0.5, 0.5, 0.5) <> help "rgb value (r,g,b) default (0.5, 0.5, 0.5)")
    <*> switch (short 'i' <> long "hide" <> help "hide clock when mouse leaves window")
    <*> option auto (short 'r' <> long "radius" <> value 1 <> help "radius r default 1")
    <*> switch (short 'o' <> long "showCursor" <> help "show cursor")
    <*> option auto (short 'a' <> long "showCursorInterval" <> value 10000 <> help "show cursor interval a default 1")

main :: IO ()
main = do
    let opts = info (bsoption <**> helper)
            ( fullDesc
                <> progDesc "Show a blank screen"
                <> header "A utility to show a blank screen, with optional clock" )
    options <- execParser opts
    let o = options ^. showCursor
    (_progName, _args) <- getArgsAndInitialize
    initialDisplayMode $= [RGBAMode, Multisampling, WithSamplesPerPixel 4]
    _window <- createWindow "bs"
    fullScreen
    t <- time
    cref <- newIORef None
    let hideCursorInterval = options ^. showCursorInterval
    if o
        then cursor $= Inherit
        else do
            cursor $= None
            void $ async $ withAsyncTimer (defaultConf & setInitDelay hideCursorInterval & setInterval hideCursorInterval) $ \ timer -> do
                passiveMotionCallback $= Just (const (do
                    cursor $= Inherit
                    reset timer))
                forever $ do
                    wait timer
                    cursor $= None                  

    tref <- newIORef (ApplicationState options t True)
    displayCallback $= display tref
    keyboardCallback $= Just keyboard
    reshapeCallback $= Just reshape
    crossingCallback $= Just (crossing tref)
    addTimerCallback 1 (timer tref)
    mainLoop

time :: IO TimeOfDay
time = do
    now <- getCurrentTime
    timezone <- getCurrentTimeZone
    return (localTimeOfDay $ utcToLocalTime timezone now)

display :: IORef ApplicationState -> DisplayCallback
display tref = do
    st <- readIORef tref
    let t = st ^. applicationTimeOfDay
    let s = floor (t ^. second)
    let r = st ^. applicationOption . clockScale
    clear [ ColorBuffer ]
    if st ^. applicationOption . clock && st ^. showClock
        then do
            preservingMatrix $ do
                scale scalex scaley scalez
                color (if st ^. applicationOption . changeColor
                    then (ccolor s)
                    else let c = st ^. applicationOption . rgb in Color3 (c ^. _1) (c ^. _2) (c ^. _3))
                renderPrimitive LineLoop (
                    mapM_ (\(x,y) -> vertex (Vertex2 x y)) (myVertices r))
                renderPrimitive Lines (mapM_ (\(x,y) -> vertex (Vertex2 x y)) (tickVertices r))
                let a1ccw = 2 * pi * fromIntegral (t ^. hour `mod` 12) / 12 + a2ccw / 12
                    a2ccw = 2 * pi * fromIntegral (t ^. minute) / 60 + a3ccw / 60
                    a3ccw = 2 * pi * fromIntegral s / 60
                    a1 = pi /2 - a1ccw
                    a2 = pi /2 - a2ccw
                    a3 = pi /2 - a3ccw
                renderPrimitive Lines (mapM_ (\(x,y) -> vertex (Vertex2 x y)) [
                    (0, 0),
                    (hourArmLength r * cos a1, hourArmLength r * sin a1),
                    (0, 0),
                    (minuteArmLength r * cos a2, minuteArmLength r * sin a2),
                    (0, 0),
                    (secondArmLength r * cos a3, secondArmLength r * sin a3)])
        else
            return ()
    flush

keyboard :: KeyboardCallback
keyboard ch _ =
    case ch of
        '\ESC' -> exitSuccess
        _ -> return ()

reshape :: ReshapeCallback
reshape (Size w h) = do
    let m = min w h
    viewport $= (Position ((w - m) `div` 2) ((h - m) `div` 2), Size m m)

crossing :: IORef ApplicationState -> CrossingCallback
crossing tref c = do
    st <- readIORef tref
    if st ^. applicationOption ^. hide
        then do
            modifyIORef' tref (& showClock .~ (case c of
                WindowLeft -> False
                WindowEntered -> True))
            postRedisplay Nothing
        else
            return ()

timer :: IORef ApplicationState -> TimerCallback
timer tref = do
    st <- readIORef tref
    let t = st ^. applicationTimeOfDay
    t' <- time
    if todHour t /= todHour t' || todMin t /= todMin t' || floor (todSec t) /= floor (todSec t')
        then do
            modifyIORef' tref (& applicationTimeOfDay .~ t')
            postRedisplay Nothing
        else
            return ()
    addTimerCallback 1 (timer tref)